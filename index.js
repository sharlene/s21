let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

//Create a function which is able to receive a single argument and add the input at the end of the users array.
function addUser() {
  let input = prompt("Enter a username you want to add at the end of the array");
  if (input) { 
    users.push(input);
    console.log("New user " + input + " has been added successfully");
  }
}

addUser();
console.log("New Array:");
console.log(users);

// Create function which is able to receive an index number as a single argument return the item accessed by its index.

function getItemByIndex() {
  let indexN = Number(prompt("Choose a number from 0-3 to see the user's name"));
  if (isNaN(indexN) || indexN < 0 || indexN > 3) { 
    console.log("Invalid index number!");
    return null;
  }
  return users[indexN];
}

let itemFound = getItemByIndex(); 
if (itemFound !== null) {
  console.log(itemFound + " has been chosen.");
}

// Create function which is able to delete the last item in the array and return the deleted item.

 function deleteLastitem() {
  const lastItem = users.pop();
  console.log("Last user " + lastItem + " has been deleted");
  console.log(users);
  return lastItem;
}

 deleteLastitem();

// Create function which is able to update a specific item in the array by its index.

function updateItem() {
  let indexSpecific = Number(prompt("Choose a number from 0-3 to change a user"));
  if (isNaN(indexSpecific) || indexSpecific < 0 || indexSpecific > 3) { 
    console.log("Invalid index number!");
    return null;
  }
  let specificItem = prompt("Please provide new username to update the existing user.");
  let originalItem = users[indexSpecific];
  users[indexSpecific] = specificItem;
  console.log(originalItem + " has been changed to " + specificItem);
}

updateItem();
console.log("Updated Array:");
console.log(users);

// Create function which is able to delete all items in the array.

function deleteAllitems() {
  users.length = 0;
}

deleteAllitems();
console.log("All users have been deleted");
console.log(users);

// Create a function which is able to check if the array is empty.

function arrIsEmpty() {
    if(users.length > 0) {
        return false
    } else {
        return true
    }
}

arrIsEmpty();
isUsersEmpty = arrIsEmpty();
console.log("The user array is empty:");
console.log(isUsersEmpty);